# api-todo-list

API REST de todo-list avec une authentification JWT développé avec Symfony 6 et API Platform

## Getting started

`docker-compose build`
`docker-compose run php composer install`
`docker-compose up -d`
`docker-compose exec php php bin/console d:d:c`
`docker-compose exec php composer install-dev`

## Phpmyadmin
`http://localhost:8081/`
Host `mysql`
User `user`
Password `password`

## OPENAPI documentation - testing the API
L'API est documenté 
`http://localhost:8080/api/docs`

Exemple d'utilisateur :
email : `user0@gmail.com`
password : `user`

Login à l'API :
- Pour tester l'API via la documentation, il faut se connecter avec un des 5 utilisateurs (disponibles dans les fixtures)  avec l'opération `/login`
- Copier le token JWT renvoyé et le coller dans le champ value du bouton Authorize (en haut a droite)
- Une fois connecté il est maintenant possible de tester les différentes opérations de l'API 

## Functionnalities

- [x] Se connecter via un identifiant (exemple : email / mot de passe)
- [x] Créer une todo-list et des tâches associées
- [x] Ajouter une tâche dans une todo-list existante
- [x] Seul le propriétaire de la todo-list peut la supprimer
- [x] Seul le propriétaire de la tâche ou de la todo-list parente peut la modifier/supprimer
- [x] Tous les autres utilisateurs peuvent voir les todo-list et tâches des autres
- [x] Pouvoir filtrer les tâches/todo-list à afficher
- [x] Bonus : Ajouter un système de status des tâches/todo-list
- [ ] Bonus : Faire un front ? Web ? Mobile ?

##  Difficulties

Je n'ai pas réussi à installer le bundle alice pour tester l'api. J'ai eu des conflits de versions avec doctrine/persistence. Je n'ai malheureusement pas trouvé de solution a cela et n'ai donc pas pu mettre les tests fonctionnels en place.

## Technicals choices

MYSQL 5.7

PHP 8.1 
- plus performant 
- pourra permettre une meilleur scalabilté et même un peu d'asynchrone avec fibers
- simplement être à jour sur une version stable

lexik/jwt
- utilisation du token jwt pour l'authentification
- stateless, c'est une solution scalable, plus performante car sans cookies stockés coté serveur et interopérable

fzaninotto/faker
- permet de générer de fausses données réalistes facilement

## Time spent

~ 6 heures






