<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use App\Repository\UserRepository;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AuthController extends AbstractController
{
    private UserRepository $userRepository;

    /**
     * AuthController Constructor
     *
     * @param UserRepository $userRepository
     * @param UserPasswordEncoderInterface $encoder
     */
    public function __construct(UserRepository $userRepository, UserPasswordEncoderInterface $encoder)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function register(Request $request): JsonResponse
    {
        $data = $request->toArray() ;

        $user = $this->userRepository->createNewUser($data);
        if (is_array($user)) {
            return new JsonResponse($user['message'], $user['code']);
        } else {
            return new JsonResponse(sprintf('User %s successfully created', $user->getUsername()));
        }
    }

    /**
    * api route redirects
    * @return Response
    */
    public function api(): Response
    {
        return new Response(sprintf("Logged in as %s", $this->getUser()->getUsername()));
    }
}
