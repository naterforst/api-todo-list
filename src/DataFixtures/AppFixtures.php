<?php

namespace App\DataFixtures;

use App\Entity\Task;
use App\Entity\TodoList;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class AppFixtures extends Fixture
{
    /**
     * @var UserPasswordHasherInterface
     */
    private UserPasswordHasherInterface $encoder;

    public function __construct(UserPasswordHasherInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager): void
    {
        $faker = Factory::create();

        /**
         * Admin
         */
        $admin = new User();
        $hash = $this->encoder->hashPassword($admin, 'admin');

        $admin->setEmail("admin@email.com")
            ->setPassword($hash)
            ->setRoles(['ROLE_ADMIN']);

        $manager->persist($admin);

        $manager->flush();

        /**
         * Users
         */
        for ($u = 0; $u < 5; $u++) {
            $user = new User();
            $hash = $this->encoder->hashPassword($user, 'user');
            $user->setEmail("user$u@email.com")
                ->setPassword($hash)
                ->setRoles(['ROLE_USER']);

            $manager->persist($user);

            /**
             * TodoLists
             */
            for ($t=0; $t < 5; $t++) {
                $todoList = new TodoList();

                $todoList->setName($faker->colorName)
                    ->setDescription($faker->realText())
                    ->setUser($user);

                $manager->persist($todoList);

                /**
                 * Tasks
                 */
                for ($t=0; $t < 5; $t++) {
                    $task = new Task();

                    $task->setName($faker->colorName)
                        ->setCreatedAt($faker->dateTime)
                        ->setUpdatedAt($faker->dateTime)
                        ->setDescription($faker->realText())
                        ->setTodoList($todoList)
                        ->setToBeDoneFor($faker->dateTime)
                        ->setUser($user)
                        ->setStatus($faker->randomElement([Task::STATUS_NEW, Task::STATUS_IN_PROGRESS, Task::STATUS_DONE]));

                    $manager->persist($task);

                }
            }
        }
        $manager->flush();
    }
}
