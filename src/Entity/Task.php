<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\DateFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Repository\TaskRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: TaskRepository::class)]
#[ApiResource (
    collectionOperations:
    [
        'get' => [
            'normalization_context' => ['groups' => ['task:collection']],
            'openapi_context' => [
                'security' => [['bearerAuth' => []]]
            ]
        ],
        'post' => [
            'openapi_context' => [
                'security' => [['bearerAuth' => []]],
                'requestBody' => [
                    'content' => [
                        'application/json' => [
                            'schema'  => [
                                'type'       => 'object',
                                'properties' =>
                                    [
                                        'name'        => ['type' => 'string'],
                                        'description' => ['type' => 'string'],
                                        'status' => ['type' => 'string'],
                                        'createdAt' => ['type' => 'date'],
                                        'updatedAt' => ['type' => 'date'],
                                        'toBeDoneFor' => ['type' => 'date'],
                                        'user' => ['type' => 'object'],
                                        'todoList' => ['type' => 'object']
                                    ],
                            ],
                            'example' => [
                                'name'        => 'New Task',
                                'description' => 'This is a new task example for openAPI!',
                                'status' => 'New',
                                'createdAt' => '2022/04/05',
                                'updatedAt' => '2022/04/05',
                                'toBeDoneFor' => '2022/04/10',
                                'user' => 'api/users/1',
                                'todoList' => 'api/todo_lists/5'
                            ],
                        ],
                    ],
                ],
            ]
        ]
    ],
    itemOperations: [
        'get'=> [
            'normalization_context' => ['groups' => ['task:item']],
            'openapi_context' => [
                'security' => [['bearerAuth' => []]]
            ],
        ],
        'put'=> [
            'openapi_context' => [
                'security' => [['bearerAuth' => []]],
                'requestBody' => [
                    'content' => [
                        'application/json' => [
                            'schema'  => [
                                'type'       => 'object',
                                'properties' =>
                                    [
                                        'name'        => ['type' => 'string'],
                                        'description' => ['type' => 'string'],
                                        'status' => ['type' => 'string'],
                                        'createdAt' => ['type' => 'date'],
                                        'updatedAt' => ['type' => 'date'],
                                        'toBeDoneFor' => ['type' => 'date'],
                                        'user' => ['type' => 'object'],
                                        'todoList' => ['type' => 'object']
                                    ],
                            ],
                            'example' => [
                                'name'        => 'Updated Task',
                                'description' => 'This is DONE !!',
                                'status' => 'Done',
                                'createdAt' => '2022/04/05',
                                'updatedAt' => '2022/04/10',
                                'toBeDoneFor' => '2022/04/10',
                                'user' => 'api/users/1',
                                'todoList' => 'api/todo_lists/5'
                            ],
                        ],
                    ],
                ],
            ],
            "security" => "is_granted('ROLE_USER') and object.getUser() == user"
        ],
        'delete' => [
            'openapi_context' => [
                'security' => [['bearerAuth' => []]]
            ],
            "security" => "is_granted('ROLE_USER') and object.getUser() == user"
        ]
    ]
)
]
#[
    ApiFilter(SearchFilter::class, properties: ['id' => 'exact', 'name' => 'partial', 'description' => 'partial', 'status' => 'partial', 'todoList' => 'exact' ]),
    ApiFilter(DateFilter::class, properties: ['createdAt', 'updatedAt', 'toBeDoneFor']),
]
class Task
{
    public const STATUS = ['New', 'In progress', 'Done'];
    public const STATUS_NEW = self::STATUS[0];
    public const STATUS_IN_PROGRESS = self::STATUS[1];
    public const STATUS_DONE = self::STATUS[2];

    #[
        ORM\Id,
        ORM\Column(type: 'integer'),
        ORM\GeneratedValue
    ]
    #[Groups(['todo-list:item', 'task:collection', 'task:item'])]
    private $id;

    #[
        ORM\Column(type: 'string', length: 255),
        Assert\NotBlank,
        Assert\Length(min: 3, max: 255)
    ]
    private string $name;

    #[ORM\Column(type: 'text', nullable: true)]
    #[Groups(['todo-list:item', 'task:collection', 'task:item'])]
    private ?string $description;

    #[
        ORM\Column(type: 'string', length: 15),
        Assert\Choice(choices: [self::STATUS_DONE, self::STATUS_IN_PROGRESS, self::STATUS_NEW], message: 'origin can be {{ choices }} only'),
        Assert\NotBlank()
    ]
    #[Groups(['todo-list:item', 'task:collection', 'task:item'])]
    private string $status = Task::STATUS_NEW;

    #[
        ORM\Column(type: 'datetime'),
    ]
    #[Groups(['todo-list:item', 'task:collection', 'task:item'])]
    private \DateTimeInterface $createdAt;

    #[
        ORM\Column(type: 'datetime', nullable: true),
    ]
    #[Groups(['todo-list:item', 'task:collection', 'task:item'])]
    private ?\DateTimeInterface $updatedAt;

    #[
        ORM\Column(type: 'datetime', nullable: true),
    ]
    #[Groups(['todo-list:item', 'task:collection', 'task:item'])]
    private ?\DateTimeInterface $toBeDoneFor;

    #[
        ORM\ManyToOne(targetEntity: User::class),
        ORM\JoinColumn(nullable: false)
    ]
    #[Groups(['task:item'])]
    private User $user;

    #[
        ORM\ManyToOne(targetEntity: TodoList::class, inversedBy: 'tasks'),
        ORM\JoinColumn(nullable: false)
    ]
    private TodoList $todoList;

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getStatus(): string
    {
        return $this->status;
    }

    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getCreatedAt(): \DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getToBeDoneFor(): ?\DateTimeInterface
    {
        return $this->toBeDoneFor;
    }

    public function setToBeDoneFor(?\DateTimeInterface $toBeDoneFor): self
    {
        $this->toBeDoneFor = $toBeDoneFor;

        return $this;
    }

    public function getUser(): User
    {
        return $this->user;
    }

    public function setUser(User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getTodoList(): ?TodoList
    {
        return $this->todoList;
    }

    public function setTodoList(?TodoList $todoList): self
    {
        $this->todoList = $todoList;

        return $this;
    }

}
