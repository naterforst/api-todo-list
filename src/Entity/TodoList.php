<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Repository\TodoListRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: TodoListRepository::class)]
#[ApiResource (
    collectionOperations:
    [
        'get' => [
            'normalization_context' => ['groups' => ['todo-list:collection']],
            'openapi_context' => [
                'security' => [['bearerAuth' => []]]
            ]
        ],
        'post' => [
            'openapi_context' => [
                'security' => [['bearerAuth' => []]],
                'requestBody' => [
                    'content' => [
                        'application/json' => [
                            'schema'  => [
                                'type'       => 'object',
                                'properties' =>
                                    [
                                        'name'        => ['type' => 'string'],
                                        'description' => ['type' => 'string'],
                                        'user' => ['type' => 'object']
                                    ],
                            ],
                            'example' => [
                                'name'        => 'New Todo',
                                'description' => 'This is a new todolist example for openAPI!',
                                'user' => 'api/users/1'
                            ],
                        ],
                    ],
                ],
            ]
        ]
    ],
    itemOperations: [
        'get'=> [
            'normalization_context' => ['groups' => ['todo-list:item']],
            'openapi_context' => [
                'security' => [['bearerAuth' => []]]
            ]
        ],
        'put' => [
            'groups' => ['todo-list:put'],
            'openapi_context' => [
                'security' => [['bearerAuth' => []]],
                'requestBody' => [
                    'content' => [
                        'application/json' => [
                            'schema'  => [
                                'type'       => 'object',
                                'properties' =>
                                    [
                                        'name'        => ['type' => 'string'],
                                        'description' => ['type' => 'string'],
                                    ],
                            ],
                            'example' => [
                                'name'        => 'Update TodoList',
                                'description' => 'This is a updated todolist example for openAPI!',
                            ],
                        ],
                    ]
                ]

            ]
        ],
        'delete' => [
            'openapi_context' => [
                'security' => [['bearerAuth' => []]]
            ],
            "security" => "is_granted('ROLE_USER') and object.getUser() == user"
        ]
    ]
)
]
#[ApiFilter(SearchFilter::class, properties: ['id' => 'exact', 'name' => 'partial', 'description' => 'partial'])]
class TodoList
{
    #[
        ORM\Id,
        ORM\GeneratedValue,
        ORM\Column(type: 'integer')
    ]
    #[Groups(['todo-list:collection','todo-list:item'])]
    private $id;

    #[
        ORM\Column(type: 'string', length: 255),
        Assert\NotBlank,
        Assert\Length(min: 3, max: 255)
    ]
    #[Groups(['todo-list:collection','todo-list:item','todo-list:put'])]
    private string $name;

    #[
        ORM\Column(type: 'text', nullable: true)
    ]
    #[Groups(['todo-list:collection','todo-list:item','todo-list:put'])]
    private ?string $description;

    #[
        ORM\ManyToOne(targetEntity: User::class),
        ORM\JoinColumn(nullable: false)
    ]
    #[Groups(['todo-list:item'])]
    private User $user;

    #[ORM\OneToMany(mappedBy: 'todoList', targetEntity: Task::class, orphanRemoval: true)]
    #[Groups(['todo-list:item'])]
    private Collection $tasks;

    public function __construct()
    {
        $this->tasks = new ArrayCollection();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getUser(): User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return Collection<int, Task>
     */
    public function getTasks(): Collection
    {
        return $this->tasks;
    }

    public function addTask(Task $task): self
    {
        if (!$this->tasks->contains($task)) {
            $this->tasks[] = $task;
            $task->setTodoList($this);
        }

        return $this;
    }

    public function removeTask(Task $task): self
    {
        if ($this->tasks->removeElement($task)) {
            // set the owning side to null (unless already changed)
            if ($task->getTodoList() === $this) {
                $task->setTodoList(null);
            }
        }

        return $this;
    }
}
